﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


using NUnit.Framework;

namespace HttpServerE2ETests
{
    [SetUpFixture]
    public class TestFixtureSetup
    {

        private static IConfigurationRoot _configuration;
        private static IServiceScopeFactory _scopeFactory;

        [OneTimeSetUp]
        public void RunBeforeAnyTests()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();

            _configuration = builder.Build();

            var services = new ServiceCollection();

            services.AddHttpClient("api_client", client => {
                var apiUrl = _configuration.GetValue<string>("API_URL");

                if (string.IsNullOrEmpty(apiUrl))
                {
                    apiUrl = @"http://localhost:8083";
                }
                
                Console.WriteLine($"Tests API:{apiUrl}");

                client.BaseAddress = new System.Uri(apiUrl);
            });

            services.AddLogging();


            _scopeFactory = services.BuildServiceProvider().GetService<IServiceScopeFactory>();
             
        } 

         

        public static IServiceScope CreateScope()
        {
            return _scopeFactory.CreateScope();
        }

        [OneTimeTearDown]
        public void Cleanup()
        {


        }


    }
}
