using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

using Microsoft.Extensions.DependencyInjection;

using NUnit.Framework;

using FluentAssertions;
using System;

namespace HttpServerE2ETests
{
    public class GatewayControllerTests
    {
        private IServiceScope _serviceScope;
        private readonly HttpClient _client;

        private readonly string InitState = "INIT";
        private readonly string RunningState = "RUNNING";
        private readonly string PausedState = "PAUSED";
        private readonly string ShutDownState = "SHUTDOWN";

        public GatewayControllerTests ()
        {
            _serviceScope = TestFixtureSetup.CreateScope();

            var factory = _serviceScope.ServiceProvider.GetService<IHttpClientFactory>();
            _client = factory.CreateClient("api_client");
        }

        protected IServiceScope ServiceScope { get; }

        [SetUp]
        public void Setup ()
        {
        }

        [Test]
        public void HttpClient_IsNotNull ()
        {
            Assert.NotNull(_client);
        }



        [Test]
        public async Task InitState_ShoulSetAutomatticallyToRunning ()
        {

            var response = await _client.PutAsync($"/?state={InitState}", default);

            var responseContent = await response.Content.ReadAsStringAsync();

            response.Should().HaveStatusCode(System.Net.HttpStatusCode.OK);

            responseContent.Should().Be(RunningState);
        }


        [Test]
        public async Task PutState_UpdatesState ()
        {

            var response = await _client.PutAsync($"/?state={PausedState}", default);

            var responseContent = await response.Content.ReadAsStringAsync();

            response.Should().HaveStatusCode(System.Net.HttpStatusCode.OK);

            responseContent.Should().Be(PausedState);
        }

        [Test]
        public async Task PausingApp_ShouldNotAddAnythingToMessages ()
        {
            var messageResponse = await _client.GetAsync($"/messages");

            var initialMessages = await messageResponse.Content.ReadAsStringAsync();

            var response = await _client.PutAsync($"/?state={PausedState}", default);

            response.Should().HaveStatusCode(System.Net.HttpStatusCode.OK);

            await Task.Delay(4000);

            var updatedMessageResponse = await _client.GetAsync($"/messages");

            var updatedMessages = await updatedMessageResponse.Content.ReadAsStringAsync();

            initialMessages.Should().Be(updatedMessages);


        }


        [Test]
        public async Task RunLogs_ShouldReturnOkResponse ()
        {

            var response = await _client.GetAsync($"/run-log");

            var responseContent = await response.Content.ReadAsStringAsync();

            response.Should().HaveStatusCode(System.Net.HttpStatusCode.OK);

            responseContent.Should().Contain(InitState).And.Contain(RunningState);
        }


        [Test]
        public async Task Shutdown_ShouldReturnThrowException ()
        {
            var initResponse = await _client.PutAsync($"/?state={ShutDownState}", default);

            initResponse.Should().HaveStatusCode(System.Net.HttpStatusCode.OK);

            var shutdownResponse = await _client.PutAsync($"/?state={ShutDownState}", default);

            shutdownResponse.Should().HaveStatusCode(System.Net.HttpStatusCode.OK);

            await Task.Delay(1000);

            var stateResponse = _client.GetAsync($"/state");

            await FluentActions.Awaiting(() => stateResponse)
                       .Should()
                       .ThrowAsync<Exception>();
        }
    }
}