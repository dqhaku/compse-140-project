#!/bin/bash
# docker version

echo "Starting applications..."

docker pull mcr.microsoft.com/dotnet/sdk:5.0

docker-compose -f ./src/docker-compose.yml build --no-cache

docker-compose -f ./src/docker-compose.yml up -d

sleep 30

HOST_IP=$(hostname -I | awk '{print $1}')

echo "Host Ip $HOST_IP"

echo "Starting e2e tests..."

docker run -it --rm -v "${PWD}/tests/HttpServerE2ETests":/src -v app-logs:/logs -e API_URL="http://$HOST_IP:8083" mcr.microsoft.com/dotnet/sdk:5.0   dotnet test /src/HttpServerE2ETests.csproj

echo "Finishing e2e tests..."

docker-compose -f ./src/docker-compose.yml down


echo "Stopping applications..."