﻿using System;
using System.Threading;
using System.Threading.Tasks;

using ApplicationCore;
using ApplicationCore.Messaging;

using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace IntermediateMessageProducer
{
    internal class AppWorker : BackgroundService
    {
        private readonly IHostApplicationLifetime _lifeTime;
        private readonly ILogger<AppWorker> _logger;
        private readonly ApplicationState _applicationState;
        private readonly MessageProducer _messageProducer;
        private readonly MessageConsumer _messageConsumer;
        private ApplicationState.State _state = ApplicationState.State.PAUSED;


        public AppWorker (IHostApplicationLifetime lifeTime, ILogger<AppWorker> logger, ApplicationState applicationState, MessageProducer messageProducer, MessageConsumer messageConsumer)
        {
            _lifeTime = lifeTime;
            _logger = logger;
            _applicationState = applicationState;

            _messageProducer = messageProducer;
            _messageConsumer = messageConsumer;
        }


        public override Task StartAsync (CancellationToken cancellationToken)
        {

            _applicationState.ListenStateChanges(MessageClient.Intermediate, state =>
             {
                 // Exit application when state set to shutdown
                 if (state == ApplicationState.State.SHUTDOWN)
                 {
                     _logger.LogWarning("Stopping application");
                     _lifeTime.StopApplication();
                     return;
                 }

                 _state = state;
                 _logger.LogInformation($"Current State:{_state}");
             });

            _messageConsumer.OnMessageReceived(async origMsg =>
            {
                var msg = $"Got {origMsg.Body}"; // new msg

                _logger.LogInformation(msg);

                await Task.Delay(1000); // delay 1 second

                _messageProducer.Publish(msg);
            });

            return base.StartAsync(cancellationToken);
        }


        protected override async Task ExecuteAsync (CancellationToken stoppingToken)
        {
            await Task.CompletedTask;

        }

    }
}