﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace ApplicationCore.Services
{
    public class FileWriter
    {
        private readonly string _filePath;

        public FileWriter (string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                throw new ArgumentNullException(nameof(filePath));
            }

            _filePath = filePath;
        }

        public Task ClearContentAsync (CancellationToken cancellationToken)
        {
            if (File.Exists(_filePath))
            {
                return File.WriteAllBytesAsync(_filePath, new byte[0], cancellationToken);
            }

            return Task.CompletedTask;
        }

        public Task AppendLineAsync (string line, CancellationToken cancellationToken)
        {
            var content = new List<string> { line };

            return File.AppendAllLinesAsync(_filePath, content, cancellationToken);
        }

    }
}
