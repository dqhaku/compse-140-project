﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ApplicationCore.Services
{
    public class FileReader
    {
        private readonly string _filePath;

        public FileReader (string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                throw new ArgumentNullException(nameof(filePath));
            }

            _filePath = filePath;
        }

        public Task<string> ReadAllTextAsync (CancellationToken cancellationToken)
        {
            if (!File.Exists(_filePath))
            {
                return default;
            }

            return File.ReadAllTextAsync(_filePath, cancellationToken);
        }
    }
}
