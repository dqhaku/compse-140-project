﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using ApplicationCore.Extensions;
using ApplicationCore.Messaging;

using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace ApplicationCore
{

    public class ApplicationState
    {
        public const string ExchangeName = "application.state";

        public const string RoutingKey = "";

        private readonly IApplicationStateClient _applicationStateClient;

        public enum State
        {
            INIT,
            PAUSED,
            RUNNING,
            SHUTDOWN
        }


        private List<(DateTime Time, State State)> _stateLogs = new List<(DateTime Time, State State)>();


        public ApplicationState (IApplicationStateClient applicationStateClient)
        {
            _applicationStateClient = applicationStateClient;

            // Create Channel
            Channel = _applicationStateClient.Connect();

        }

        private IModel Channel { get; }


        public IReadOnlyCollection<(DateTime Time, State State)> GetStateLogs ()
        {
            return _stateLogs.AsReadOnly();
        }

        public State CurrentState { get; private set; } = State.PAUSED;

        private Action<State> OnStateChange { get; set; }

        public void ListenStateChanges (MessageClient client, Action<State> onStateChange)
        {
            var queueName = $"{ExchangeName}.{client.QueueName}";

            // Declare exchange

            Channel.ExchangeDeclare(exchange: ExchangeName, type: ExchangeType.Fanout);

            // Declare queue

            Channel.QueueDeclare(queueName, false, false, false, null);

            // Bind quues to exchange
            Channel.QueueBind(queueName, ExchangeName, RoutingKey, null);

            OnStateChange = onStateChange;

            var consumer = new AsyncEventingBasicConsumer(Channel);

            Channel.BasicQos(0, 1, false);
            Channel.BasicConsume(queueName, false, consumer);

            consumer.Received += async (s, e) =>
            {

                var body = e.Body.ToArray();


                Console.WriteLine(body);

                var message = Encoding.UTF8.GetString(body);

                var currentState = (State)Enum.Parse(typeof(State), message);

                Channel.BasicAck(e.DeliveryTag, false);

                if (CurrentState == currentState)
                {
                    return;
                }

                CurrentState = currentState;


                onStateChange?.Invoke(currentState);

                await Task.CompletedTask;

            };
        }

        public void UpdateState (State state)
        {
            if (state == CurrentState)
            {
                return;
            }

            CurrentState = state;

            var time = DateTime.UtcNow;

            _stateLogs.Add((time, CurrentState));

            PublishApplicationState(CurrentState);

            if (CurrentState == State.INIT)
            {
                UpdateState(State.RUNNING);
            }

        }

        private void PublishApplicationState (State state)
        {

            var body = Encoding.UTF8.GetBytes(state.ToString());


            Channel.BasicPublish(exchange: ExchangeName,
                                 routingKey: RoutingKey,
                                 basicProperties: null,
                                 body: body);
        }


    }
}
