﻿using System;
using System.Text;

using RabbitMQ.Client;

namespace ApplicationCore.Messaging
{
    public class MessageProducer
    {
        private readonly IMessageServiceClient _messageServiceClient;
        private readonly MessagingTopic _messagingTopic;
        private readonly IModel _channel;
        private readonly IBasicProperties _basicProperties;

        public MessageProducer (IMessageServiceClient messageServiceClient, MessagingTopic messagingTopic)
        {
            _messageServiceClient = messageServiceClient;
            _messagingTopic = messagingTopic;

            //// Create Channel 

            _channel = _messageServiceClient.Connect();
            
            //// Create Exchange with Topic Exchange Type

            _channel.ExchangeDeclare(messagingTopic.ExchangeName, ExchangeType.Topic);

            _basicProperties =  _channel.CreateBasicProperties();

            _basicProperties.Persistent = true;

        }


        public void Publish (string message)
        {

            if (string.IsNullOrEmpty(message))
            {
                throw new ArgumentException("Message body can not be empty!", nameof(message));
            }

           
            var messagebuffer = Encoding.Default.GetBytes(message);

            _channel.BasicPublish(_messagingTopic.ExchangeName, _messagingTopic.RoutingKey, _basicProperties, messagebuffer);

        }
    }
}
