﻿using System;
using System.Collections.Generic;
using System.Text;

using RabbitMQ.Client;

namespace ApplicationCore.Messaging
{

    public class MessageServiceClient : IMessageServiceClient, IDisposable
    {
        private readonly ConnectionFactory _connectionFactory;
        private IConnection _connection;
        
        public MessageServiceClient (ConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory ?? throw new ArgumentNullException(nameof(connectionFactory));

        }

        public IModel Connect ()
        {
            _connection = _connectionFactory.CreateConnection();

            return _connection.CreateModel();

        }


        public void Dispose ()
        {
            _connection?.Close();
            _connection?.Dispose();

        }
    }
}
