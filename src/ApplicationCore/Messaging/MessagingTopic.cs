﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Messaging
{
        public class MessagingTopic
    {
        private static MessagingTopic _myO;

        public static MessagingTopic MyO
        {
            get
            {
                return _myO ??= new MessagingTopic("my.o", "my.o", "my.o.key"); ;

            }

        }
        private static MessagingTopic _myI;

        public static MessagingTopic MyI
        {
            get
            {
                return _myI ??= new MessagingTopic("my.i", "my.i", "my.i.key");

            }

        }

        private MessagingTopic (string queueName, string exchangeName, string routingKey)
        {
            QueueName = queueName;
            ExchangeName = exchangeName;
            RoutingKey = routingKey;
        }

        public string QueueName { get; }
        public string ExchangeName { get; }
        public string RoutingKey { get; }
    }

}
