﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace ApplicationCore.Messaging
{
    public class MessageConsumer
    {

        private readonly IMessageServiceClient _messageServiceClient;
        private readonly MessagingTopic _messagingTopic;
        private readonly IModel _channel;
        private readonly string _queueName;
        public MessageConsumer (IMessageServiceClient messageServiceClient, MessagingTopic messagingTopic, MessageClient client)
        {
            _messageServiceClient = messageServiceClient;
            _messagingTopic = messagingTopic;
            _queueName = $"{messagingTopic.QueueName}.{client.QueueName}";

            //// Create Channel 

            _channel = _messageServiceClient.Connect();

            //// Create Exchange with Topic Exchange Type

            _channel.ExchangeDeclare(messagingTopic.ExchangeName, ExchangeType.Topic);

            //// Create Queue

            _channel.QueueDeclare(_queueName, true, false, false, null);

            //// Bind Queue to Exchanges

            _channel.QueueBind(_queueName, messagingTopic.ExchangeName, messagingTopic.RoutingKey);
        }

        public void OnMessageReceived (Action<Message> onMessageReceived)
        {

            var consumer = new AsyncEventingBasicConsumer(_channel);

            _channel.BasicQos(0, 1, false);
            _channel.BasicConsume(_queueName, false, consumer);

            consumer.Received += async (s, e) =>
            {

                var body = e.Body.ToArray();

                var messageBody = Encoding.UTF8.GetString(body);

                var message = new Message(_messagingTopic, messageBody);


                _channel.BasicAck(e.DeliveryTag, false);

                onMessageReceived?.Invoke(message);

                await Task.CompletedTask;

            };
        }
    }
}
