﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Messaging
{
    public class MessageClient
    {

        public static MessageClient Original => new MessageClient("orig");
        public static MessageClient Intermediate => new MessageClient("imed");
        public static MessageClient Observer => new MessageClient("obse");

        private MessageClient (string queueName)
        {
            QueueName = queueName;
        }

        public string QueueName { get; }
    }
}
