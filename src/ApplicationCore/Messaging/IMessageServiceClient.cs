﻿namespace ApplicationCore.Messaging
{
    public interface IMessageServiceClient
    {
        RabbitMQ.Client.IModel Connect ();
    }
}