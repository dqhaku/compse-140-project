﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Messaging
{
    public class Message : IMessage
    {
        public Message (MessagingTopic topic, string messageBody)
        {
            Topic = topic ?? throw new ArgumentNullException(nameof(topic));

            if (string.IsNullOrEmpty(messageBody))
            {
                throw new ArgumentException($"'{nameof(messageBody)}' cannot be null or empty.", nameof(messageBody));
            }

            Body = messageBody;

        }


        public MessagingTopic Topic { get; }

        public string Body { get; }

        public DateTime Time { get; } = DateTime.UtcNow;

        public string GetISO8601TimeString () => Time.ToString("yyyy-MM-ddTHH:mm:ss.fffK");

        public override string ToString ()
        {
            // Format message as {timestamp} Topic {topic}: {message}
            return $"{GetISO8601TimeString()} Topic {Topic.ExchangeName}: {Body}";
        }
    }
}
