﻿namespace ApplicationCore.Messaging
{
    public interface IMessage
    {
        MessagingTopic Topic { get; }

        string Body { get; }

        System.DateTime Time { get; }

        string GetISO8601TimeString ();

    }
}