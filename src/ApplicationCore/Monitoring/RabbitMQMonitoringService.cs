﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using ApplicationCore.Messaging;

using RabbitMQ.Client;

namespace ApplicationCore.Monitoring
{
    public class RabbitMQMonitoringService : IDisposable
    {
        private readonly ConnectionFactory _connectionFactory;
        private readonly IConnection _connection;

        private readonly HttpClient _client;

        public RabbitMQMonitoringService (ConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory ?? throw new ArgumentNullException(nameof(connectionFactory));

            _connection = _connectionFactory.CreateConnection();

            _client = new HttpClient();


            _client.BaseAddress = new Uri($"http://{_connectionFactory.Endpoint.HostName}:15672");

            var byteArray = Encoding.ASCII.GetBytes($"{connectionFactory.UserName}:{connectionFactory.Password}");

            _client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

        }

        public async Task<string> GetOverviewAsync (CancellationToken cancellationToken = default)
        {
            var response = await _client.GetAsync("api/overview", cancellationToken).ConfigureAwait(false);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsStringAsync();
        }


        public async Task<string> GetNodesAsync (CancellationToken cancellationToken = default)
        {
            var response = await _client.GetAsync("api/nodes", cancellationToken).ConfigureAwait(false);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsStringAsync();
        }


        public void Dispose ()
        {
            _client?.Dispose();
            _connection?.Dispose();
        }
    }
}
