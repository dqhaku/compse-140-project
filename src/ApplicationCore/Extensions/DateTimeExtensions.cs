﻿using System;
using System.Globalization;
namespace ApplicationCore.Extensions
{
    public static class DateTimeExtensions
    {
        private const string _dateTimeFormat = "yyyy-MM-ddTHH:mm:ss.fffK";
        /// <summary>
        /// Formats datetime to t YYYY-MM-DDThh:mm:ss.sssZ (ISO 8601)
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static string ToISO8601FormattedString (this DateTime dateTime)
        {
            return dateTime.ToString(_dateTimeFormat);
        }


    }
}
