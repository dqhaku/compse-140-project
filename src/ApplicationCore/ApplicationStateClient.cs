﻿using System;
using System.Text;

using ApplicationCore.Messaging;

using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace ApplicationCore
{
    public interface IApplicationStateClient : IMessageServiceClient
    {

    }
    public class ApplicationStateClient : MessageServiceClient, IApplicationStateClient
    {
        public ApplicationStateClient (ConnectionFactory connectionFactory) : base(connectionFactory)
        {
        }
    }
}
