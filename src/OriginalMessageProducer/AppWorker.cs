﻿using System;
using System.Threading;
using System.Threading.Tasks;

using ApplicationCore;
using ApplicationCore.Messaging;

using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace OriginalMessageProducer
{
    internal class AppWorker : BackgroundService
    {
        private readonly IHostApplicationLifetime _lifeTime;
        private readonly ILogger<AppWorker> _logger;
        private readonly ApplicationState _applicationState;
        private readonly MessageProducer _messageProducer;

        private ApplicationState.State _state = ApplicationState.State.PAUSED;
        private int _currentCounter = 1;
        private const short _delayMiliseconds = 3000;

        public AppWorker (IHostApplicationLifetime lifeTime, ILogger<AppWorker> logger, ApplicationState applicationState, MessageProducer messageProducer)
        {
            _lifeTime = lifeTime;
            _logger = logger;
            _applicationState = applicationState;

            _messageProducer = messageProducer;


        }


        public override Task StartAsync (CancellationToken cancellationToken)
        {

            _applicationState.ListenStateChanges(MessageClient.Original, state =>
             {
                 // Exit application when state set to shutdown
                 if (state == ApplicationState.State.SHUTDOWN)
                 {
                     _logger.LogWarning("Stopping application");
                     _lifeTime.StopApplication();
                     return;
                 }

                 // Reset counter when state changes to INIT

                 if (state == ApplicationState.State.INIT)
                 {
                     _currentCounter = 1; // Reset counter to 1
                     return;
                 }

                 _state = state;
                 _logger.LogInformation($"Current State:{_state}");
             });

            return base.StartAsync(cancellationToken);
        }


        protected override async Task ExecuteAsync (CancellationToken cancellationToken)
        {
            await Task.Delay(1000); // Initial delay

            // While application request is not cancelled publish messages
            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    await PublishMessageAsync(cancellationToken);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, nameof(ExecuteAsync));
                }
            }

        }

        /// <summary>
        /// Publishes messages from Original Message Producer
        /// </summary>
        /// <returns></returns>
        private async Task PublishMessageAsync (CancellationToken cancellationToken)
        {

            if (_state == ApplicationState.State.RUNNING)
            {
                var message = $"MSG_{_currentCounter}";

                _messageProducer.Publish(message);

                _logger.LogInformation($"Publishing message. {message}");

                _currentCounter++; // Increase counter

                await Task.Delay(_delayMiliseconds, cancellationToken); // Delay between messages
            }

        }

    }
}