﻿using System;
using System.Threading;
using System.Threading.Tasks;

using ApplicationCore;
using ApplicationCore.Messaging;
using ApplicationCore.Services;

using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ObserverMessageConsumer
{
    internal class AppWorker : BackgroundService
    {
        private readonly IHostApplicationLifetime _lifeTime;
        private readonly ILogger<AppWorker> _logger;
        private readonly ApplicationState _applicationState;
        private readonly FileWriter _fileWriter;
        private readonly MessageConsumer _originalMessageConsumer;
        private readonly MessageConsumer _intermediateMessageConsumer;

        private ApplicationState.State _state = ApplicationState.State.PAUSED;


        public AppWorker (IHostApplicationLifetime lifeTime, ILogger<AppWorker> logger, ApplicationState applicationState, IMessageServiceClient messageServiceClient, FileWriter fileWriter)
        {
            _lifeTime = lifeTime;
            _logger = logger;
            _applicationState = applicationState;
            _fileWriter = fileWriter;
            _originalMessageConsumer = new MessageConsumer(messageServiceClient, MessagingTopic.MyO, MessageClient.Observer);
            _intermediateMessageConsumer = new MessageConsumer(messageServiceClient, MessagingTopic.MyI, MessageClient.Observer);
        }


        public override async Task StartAsync (CancellationToken cancellationToken)
        {

            // Clear file content when app starts
            await _fileWriter.ClearContentAsync(cancellationToken);

            _applicationState.ListenStateChanges(MessageClient.Observer, async state =>
            {

                // Exit application when state set to shutdown
                if (state == ApplicationState.State.SHUTDOWN)
                {
                    _logger.LogWarning("Stopping application");
                    _lifeTime.StopApplication();
                    return;
                }

                // Clear file content when app state sets to INIT

                if (state == ApplicationState.State.INIT)
                {
                    await _fileWriter.ClearContentAsync(cancellationToken);
                }


                _state = state;
                _logger.LogInformation($"Current State:{_state}");
            });

            _originalMessageConsumer.OnMessageReceived(async msg =>
            {
                _logger.LogInformation(msg.Body);

                await _fileWriter.AppendLineAsync(msg.ToString(), cancellationToken);


            });

            _intermediateMessageConsumer.OnMessageReceived(async msg =>
            {
                _logger.LogInformation(msg.Body);

                await _fileWriter.AppendLineAsync(msg.ToString(), cancellationToken);


            });


            await base.StartAsync(cancellationToken);
        }


        protected override async Task ExecuteAsync (CancellationToken stoppingToken)
        {
            await Task.CompletedTask;

        }

    }
}