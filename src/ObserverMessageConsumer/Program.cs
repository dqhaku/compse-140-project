﻿using ApplicationCore;
using ApplicationCore.Messaging;
using ApplicationCore.Services;


using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Microsoft.Extensions.Hosting;

using RabbitMQ.Client;


namespace ObserverMessageConsumer
{
    public class Program
    {
        public static void Main (string[] args)
        {
            // Create host builder & run
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder (string[] args)
        {

            // Create host with default builder
            // This will add environment variables appsettings.json and default loggers to host
            var hostBuilder = Host.CreateDefaultBuilder(args)
             .ConfigureServices((hostContext, services) =>
             {
                 IConfiguration Configuration = hostContext.Configuration;


                 // Add ConnectionFactory to ServiceCollection
                 services.AddSingleton(sp =>
                 {

                     var hostName = Configuration.GetValue<string>("RABBITMQ_HOSTNAME");
                     var userName = Configuration.GetValue<string>("RABBITMQ_USERNAME");
                     var password = Configuration.GetValue<string>("RABBITMQ_PASSWORD");

                     return new ConnectionFactory()
                     {

                         HostName = hostName,
                         UserName = userName,
                         Password = password,
                         DispatchConsumersAsync = true
                     };

                 });

                 // Add FileWriter to ServiceCollection
                 services.AddSingleton(sp =>
                 {
                     var filePath = Configuration.GetValue<string>("CONTENT_FILE_PATH");
                     return new FileWriter(filePath);
                 });

                 // Add MessageServiceClient to ServiceCollection

                 services.AddSingleton<IMessageServiceClient, MessageServiceClient>();

                 // Add ApplicationStateClient to ServiceCollection

                 services.AddSingleton<IApplicationStateClient, ApplicationStateClient>();

                 // Add ApplicationState to ServiceCollection

                 services.AddSingleton<ApplicationState>();


                 // Add AppWorker to ServiceCollection
                 services.AddHostedService<AppWorker>();

             });

            return hostBuilder;

        }

    }
}
