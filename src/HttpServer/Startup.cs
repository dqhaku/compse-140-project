using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ApplicationCore;
using ApplicationCore.Messaging;
using ApplicationCore.Monitoring;
using ApplicationCore.Services;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

using RabbitMQ.Client;

namespace HttpServer
{
    public class Startup
    {
        public Startup (IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services)
        {


            services.AddControllers().AddJsonOptions(opts =>
            {
                // Application is using enum for AppState, Add EnumString Converter for json 
                var enumConverter = new System.Text.Json.Serialization.JsonStringEnumConverter();
                opts.JsonSerializerOptions.Converters.Add(enumConverter);
            });

            // Add swagger

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "HttpServer", Version = "v1" });
            });

            // Add MessageServiceClientOptions to services

            // Add ConnectionFactory to ServiceCollection
            services.AddSingleton(sp =>
            {

                var hostName = Configuration.GetValue<string>("RABBITMQ_HOSTNAME");
                var userName = Configuration.GetValue<string>("RABBITMQ_USERNAME");
                var password = Configuration.GetValue<string>("RABBITMQ_PASSWORD");

                return new ConnectionFactory()
                {

                    HostName = hostName,
                    UserName = userName,
                    Password = password,
                    DispatchConsumersAsync = true
                };

            });

            services.AddSingleton<RabbitMQMonitoringService>();

            // Add FileReader to ServiceCollection
            services.AddSingleton(sp =>
            {
                var filePath = Configuration.GetValue<string>("CONTENT_FILE_PATH");
                return new FileReader(filePath);
            });

            // Add IMessageServiceClient to services

            // services.AddSingleton<IMessageServiceClient>(sp => MessagingClientFactory.Create(sp.GetRequiredService<MessageServiceClientOptions>()));

            services.AddSingleton<IApplicationStateClient, ApplicationStateClient>();

            // Add ApplicationState to services

            services.AddSingleton<ApplicationState>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "HttpServer v1"));
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });


            var applicationState = app.ApplicationServices.GetRequiredService<ApplicationState>();
            applicationState.UpdateState(ApplicationState.State.INIT);
        }
    }
}
