﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using ApplicationCore;
using ApplicationCore.Extensions;
using ApplicationCore.Services;
using ApplicationCore.Monitoring;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace HttpServer.Controllers
{
    [ApiController]
    public class GatewayController : ControllerBase
    {
        private readonly IHostApplicationLifetime _lifeTime;
        private readonly ILogger<GatewayController> _logger;
        private readonly ApplicationState _applicationState;
        private readonly FileReader _fileReader;
        private readonly RabbitMQMonitoringService _rabbitMQMonitoringService;

        public GatewayController (IHostApplicationLifetime lifeTime, ILogger<GatewayController> logger, ApplicationState applicationState, FileReader fileReader, RabbitMQMonitoringService rabbitMQMonitoringService)
        {
            _lifeTime = lifeTime;
            _logger = logger;
            _applicationState = applicationState;
            _fileReader = fileReader;
            _rabbitMQMonitoringService = rabbitMQMonitoringService;
        }



        [HttpGet("/messages")]
        public Task<string> GetMessages (CancellationToken cancellationToken)
        {
            return _fileReader.ReadAllTextAsync(cancellationToken);

        }

        [HttpGet("/state")]
        public string GetState ()
        {
            return _applicationState.CurrentState.ToString();
        }

        [HttpPut("/")]
        public ActionResult<string> SetState (ApplicationState.State state)
        {
            _applicationState.UpdateState(state);
            if (state == ApplicationState.State.SHUTDOWN)
            {
                _logger.LogWarning("Stopping application");
                _lifeTime.StopApplication();
            }
            return _applicationState.CurrentState.ToString();
        }

        [HttpGet("/run-log")]
        public string GetStateLogs ()
        {
            return string.Join(Environment.NewLine, _applicationState.GetStateLogs().Select(log =>
            {
                return $"{log.Time.ToISO8601FormattedString()}: {log.State}";
            }));
        }

        [HttpGet("/node-statistic")]
        public async Task<ActionResult> GetRabbitMQOverview (CancellationToken cancellationToken)
        {
            var content = await _rabbitMQMonitoringService.GetOverviewAsync(cancellationToken);

            return new JsonResult(content);
        }


        [HttpGet("/nodes")]
        public async Task<ActionResult> GetRabbitMQNodes (CancellationToken cancellationToken)
        {
            var content = await _rabbitMQMonitoringService.GetNodesAsync(cancellationToken);

            return new JsonResult(content);
        }
    }
}
