To simplify testing process test-applications.sh script file has been added to project.
This script executes docker-compose up first and makes sure all the containers are running before testing start. Testing done also inside DotNet SDK container to reduce amount of work needs to be done to run tests.
Running following commands should be enough to run all the applications and start e2e tests.

git clone https://gitlab.com/dqhaku/compse-140-project.git

cd compse-140-project/

chmod +X test-application.sh

./test-application.sh

All the apps run inside containers and tests performed similar manner during development process. Having Docker Desktop for Windows under WSL created certain challenges and Gitlab CI test build & test stages failed a lot of times.
To simplify testing process **test-applications.sh** script file has been added to project.

This script executes docker-compose up first and makes sure all the containers are running before testing start. Testing done also inside DotNet SDK container to reduce amount of work needs to be done to run tests.

Running following commands should be enough to run all the applications and start e2e tests.

`git clone https://gitlab.com/dqhaku/compse-140-project.git`

`cd compse-140-project/`

`chmod +X test-application.sh`

`./test-application.sh`

All the apps run inside containers and tests performed similar manner during development process. Having Docker Desktop for Windows under WSL created certain challenges and Gitlab CI test build &amp; test stages failed a lot of times.
